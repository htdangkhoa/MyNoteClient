package com.example.dangkhoa.mynote;

/**
 * Created by dangkhoa on 03/01/2017.
 */

public class Note {
    public String Title, Content, Id, Status;

    public Note(String title, String content, String id, String status) {
        Title = title;
        Content = content;
        Id = id;
        Status = status;
    }
}

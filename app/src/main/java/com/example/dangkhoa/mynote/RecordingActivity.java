package com.example.dangkhoa.mynote;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class RecordingActivity extends AppCompatActivity {
    ImageView status;
    ImageButton btnStart, btnStop;
    TextView txtvTime;

    File folder = new File(Environment.getExternalStorageDirectory() + "/MyNote");
    MediaRecorder recorder = new MediaRecorder();
    TimeCounter  timer;

    String filename = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording);

        setTitle("Note Recorder");

        initialize();

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status.setImageResource(R.drawable.record_recording);

                DateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm");

                filename = folder.getPath() + "/MyNote-" + df.format(Calendar.getInstance().getTime())+".3gpp";

                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                recorder.setOutputFile(filename);
                try {
                    recorder.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                recorder.start();

                timer = new TimeCounter(62*1000, 1000);
                timer.start();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status.setImageResource(R.drawable.record_logo);

                recorder.stop();
                recorder.reset();
                recorder.release();

                timer.onFinish();
                timer.cancel();

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("RecordFilePath", filename);
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), CreateAndEditActivity.class);
                startActivityForResult(intent, 0);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void initialize(){
        status = (ImageView) findViewById(R.id.status_record);
        btnStart = (ImageButton) findViewById(R.id.btn_start_record);
        btnStop = (ImageButton) findViewById(R.id.btn_stop_record);
        txtvTime = (TextView) findViewById(R.id.txtv_record_time);

        if (!folder.exists()){
            folder.mkdir();
        }
    }

    class TimeCounter extends CountDownTimer {
        // this is my seconds up counter
        int countUpTimer, min;
        public TimeCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            countUpTimer = 0;
            min = 0;
        }

        @Override
        public void onTick(long l) {
            countUpTimer += 1;

            if (countUpTimer == 60){
                countUpTimer = 0;
                min += 1;
            }

            if (countUpTimer < 10 && min < 10){
                txtvTime.setText("0" + String.valueOf(min) + ":0" + String.valueOf(countUpTimer));
            }else if(countUpTimer < 10 && min >= 10){
                txtvTime.setText(String.valueOf(min) + ":0" + String.valueOf(countUpTimer));
            }else if(countUpTimer >= 10 && min < 10){
                txtvTime.setText("0" + String.valueOf(min) + ":" + String.valueOf(countUpTimer));
            }else {
                txtvTime.setText(String.valueOf(min) + ":" + String.valueOf(countUpTimer));
            }
        }

        @Override
        public void onFinish() {
            //reset counter to 0 if you want
            countUpTimer=0;
            min = 0;
            txtvTime.setText("00:00");
        }
    }
}

package com.example.dangkhoa.mynote;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64OutputStream;
import android.widget.Toast;

import com.loopj.android.http.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;

/**
 * Created by dangkhoa on 03/01/2017.
 */

public class FunctionClass {
    public static JSONArray getJSON (String string) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(string);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonArray;
    }

    public static String imageToBase64(Bitmap imageBitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static Bitmap getResizedBitmap(Bitmap image) {
        Bitmap scaledBitmap = null;

        Matrix matrix = new Matrix();
        matrix.postRotate(0);
        Bitmap rotatedBitmap = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, false);

        if (image.getWidth() > image.getHeight()){
            scaledBitmap = Bitmap.createScaledBitmap(rotatedBitmap, 1280, 720, false);
        }else if(image.getWidth() < image.getHeight()){
            scaledBitmap = Bitmap.createScaledBitmap(rotatedBitmap, 720, 1280, false);
        }else {
            scaledBitmap = Bitmap.createScaledBitmap(rotatedBitmap, 720, 720, false);
        }

        return scaledBitmap;
    }

    public static String fileToBase64(String path){
        InputStream inputStream = null;
        File file = new File(path);
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {
            inputStream = new FileInputStream(file.getAbsolutePath());

            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            inputStream = new FileInputStream(file.getAbsolutePath());
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return output.toString();
    }
}

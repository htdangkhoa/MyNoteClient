package com.example.dangkhoa.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.dangkhoa.mynote.FunctionClass;
import com.example.dangkhoa.mynote.ListAdapter;
import com.example.dangkhoa.mynote.LoginActivity;
import com.example.dangkhoa.mynote.MainActivity;
import com.example.dangkhoa.mynote.Note;
import com.example.dangkhoa.mynote.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 02/01/2017.
 */

public class HomeFragment extends Fragment {
    public ListView lstvNote;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        lstvNote = (ListView) rootView.findViewById(R.id.lstvNote);
        final ArrayList<Note> arrNotes = new ArrayList<>();
        final ListAdapter adapter = new ListAdapter(getActivity(), R.layout.activity_line_note, arrNotes);

        registerForContextMenu(lstvNote);

        final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(LoginActivity.base_host + "notelist?email=" + MainActivity.settings.getString("email", ""), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);

                if (result.length() != 0){
                    JSONArray array = FunctionClass.getJSON(new String(responseBody));
                    String[] strArr = new String[array.length()];

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            strArr[i] = array.getString(i);

                            JSONObject jo = new JSONObject(array.getString(i));

                            arrNotes.add(new Note(String.valueOf(jo.get("Title")), String.valueOf(jo.get("Content")), String.valueOf(jo.get("id")), "Status: " + String.valueOf(jo.get("Status"))));
                            lstvNote.setAdapter(adapter);

                            System.out.println(jo.get("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                progressDialog.hide();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getActivity(), new String(responseBody), Toast.LENGTH_SHORT).show();
                progressDialog.hide();
            }
        });
        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Note note = (Note) lstvNote.getItemAtPosition(info.position);

        int id =  item.getItemId();

        switch (id){
            case R.id.context_menu_delete: {
                Toast.makeText(getActivity(), "Do delete", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.context_menu_share: {
                Intent sendMailIntent = new Intent(Intent.ACTION_SEND);
                sendMailIntent.putExtra(Intent.EXTRA_SUBJECT, "My Note");
                sendMailIntent.putExtra(Intent.EXTRA_TEXT, LoginActivity.base_host + "share?email=" + MainActivity.settings.getString("email", "") + "&id=" + note.Id);
                sendMailIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendMailIntent, "Share link Using"));
                break;
            }
        }

        return true;
    }
}
